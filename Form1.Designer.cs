namespace Zylinderberechnung_GUI
{
    partial class Zylinderberechnung
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtn_grundflaeche = new System.Windows.Forms.RadioButton();
            this.rbtn_Umfang = new System.Windows.Forms.RadioButton();
            this.rbtn_Volumen = new System.Windows.Forms.RadioButton();
            this.rbtn_Mantelflaeche = new System.Windows.Forms.RadioButton();
            this.rbtn_Oberflaeche = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_input_Hoehe = new System.Windows.Forms.TextBox();
            this.txt_input_radius = new System.Windows.Forms.TextBox();
            this.lbl_hoehe = new System.Windows.Forms.Label();
            this.lbl_radius = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_end = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbtn_grundflaeche
            // 
            this.rbtn_grundflaeche.AutoSize = true;
            this.rbtn_grundflaeche.Location = new System.Drawing.Point(32, 38);
            this.rbtn_grundflaeche.Name = "rbtn_grundflaeche";
            this.rbtn_grundflaeche.Size = new System.Drawing.Size(98, 20);
            this.rbtn_grundflaeche.TabIndex = 0;
            this.rbtn_grundflaeche.Text = "Grundfläche";
            this.rbtn_grundflaeche.UseVisualStyleBackColor = true;
            this.rbtn_grundflaeche.CheckedChanged += new System.EventHandler(this.rbtn_grundflaeche_CheckedChanged);
            // 
            // rbtn_Umfang
            // 
            this.rbtn_Umfang.AutoSize = true;
            this.rbtn_Umfang.Location = new System.Drawing.Point(166, 38);
            this.rbtn_Umfang.Name = "rbtn_Umfang";
            this.rbtn_Umfang.Size = new System.Drawing.Size(73, 20);
            this.rbtn_Umfang.TabIndex = 1;
            this.rbtn_Umfang.Text = "Umfang";
            this.rbtn_Umfang.UseVisualStyleBackColor = true;
            this.rbtn_Umfang.CheckedChanged += new System.EventHandler(this.rbtn_Umfang_CheckedChanged);
            // 
            // rbtn_Volumen
            // 
            this.rbtn_Volumen.AutoSize = true;
            this.rbtn_Volumen.Location = new System.Drawing.Point(274, 38);
            this.rbtn_Volumen.Name = "rbtn_Volumen";
            this.rbtn_Volumen.Size = new System.Drawing.Size(79, 20);
            this.rbtn_Volumen.TabIndex = 2;
            this.rbtn_Volumen.Text = "Volumen";
            this.rbtn_Volumen.UseVisualStyleBackColor = true;
            this.rbtn_Volumen.CheckedChanged += new System.EventHandler(this.rbtn_Volumen_CheckedChanged);
            // 
            // rbtn_Mantelflaeche
            // 
            this.rbtn_Mantelflaeche.AutoSize = true;
            this.rbtn_Mantelflaeche.Location = new System.Drawing.Point(391, 38);
            this.rbtn_Mantelflaeche.Name = "rbtn_Mantelflaeche";
            this.rbtn_Mantelflaeche.Size = new System.Drawing.Size(102, 20);
            this.rbtn_Mantelflaeche.TabIndex = 3;
            this.rbtn_Mantelflaeche.Text = "Mantelfläche";
            this.rbtn_Mantelflaeche.UseVisualStyleBackColor = true;
            this.rbtn_Mantelflaeche.CheckedChanged += new System.EventHandler(this.rbtn_Mantelflaeche_CheckedChanged);
            // 
            // rbtn_Oberflaeche
            // 
            this.rbtn_Oberflaeche.AutoSize = true;
            this.rbtn_Oberflaeche.Location = new System.Drawing.Point(526, 38);
            this.rbtn_Oberflaeche.Name = "rbtn_Oberflaeche";
            this.rbtn_Oberflaeche.Size = new System.Drawing.Size(92, 20);
            this.rbtn_Oberflaeche.TabIndex = 4;
            this.rbtn_Oberflaeche.Text = "Oberfläche";
            this.rbtn_Oberflaeche.UseVisualStyleBackColor = true;
            this.rbtn_Oberflaeche.CheckedChanged += new System.EventHandler(this.rbtn_Oberflaeche_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_input_Hoehe);
            this.groupBox1.Controls.Add(this.txt_input_radius);
            this.groupBox1.Controls.Add(this.lbl_hoehe);
            this.groupBox1.Controls.Add(this.lbl_radius);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(32, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(225, 149);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Eingaben";
            // 
            // txt_input_Hoehe
            // 
            this.txt_input_Hoehe.Location = new System.Drawing.Point(107, 74);
            this.txt_input_Hoehe.Name = "txt_input_Hoehe";
            this.txt_input_Hoehe.Size = new System.Drawing.Size(100, 22);
            this.txt_input_Hoehe.TabIndex = 3;
            this.txt_input_Hoehe.Visible = false;
            // 
            // txt_input_radius
            // 
            this.txt_input_radius.Location = new System.Drawing.Point(107, 33);
            this.txt_input_radius.Name = "txt_input_radius";
            this.txt_input_radius.Size = new System.Drawing.Size(100, 22);
            this.txt_input_radius.TabIndex = 2;
            this.txt_input_radius.Visible = false;
            // 
            // lbl_hoehe
            // 
            this.lbl_hoehe.AutoSize = true;
            this.lbl_hoehe.Location = new System.Drawing.Point(22, 79);
            this.lbl_hoehe.Name = "lbl_hoehe";
            this.lbl_hoehe.Size = new System.Drawing.Size(49, 16);
            this.lbl_hoehe.TabIndex = 1;
            this.lbl_hoehe.Text = "Höhe:";
            this.lbl_hoehe.Visible = false;
            // 
            // lbl_radius
            // 
            this.lbl_radius.AutoSize = true;
            this.lbl_radius.Location = new System.Drawing.Point(19, 33);
            this.lbl_radius.Name = "lbl_radius";
            this.lbl_radius.Size = new System.Drawing.Size(61, 16);
            this.lbl_radius.TabIndex = 0;
            this.lbl_radius.Text = "Radius:";
            this.lbl_radius.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 269);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ergebnis:";
            // 
            // txt_output
            // 
            this.txt_output.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txt_output.Location = new System.Drawing.Point(129, 269);
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.Size = new System.Drawing.Size(100, 22);
            this.txt_output.TabIndex = 7;
            this.txt_output.TabStop = false;
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(32, 335);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 33);
            this.btn_ok.TabIndex = 8;
            this.btn_ok.TabStop = false;
            this.btn_ok.Text = "&OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(164, 335);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(75, 33);
            this.btn_clear.TabIndex = 9;
            this.btn_clear.TabStop = false;
            this.btn_clear.Text = "&Löschen";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // btn_end
            // 
            this.btn_end.BackColor = System.Drawing.Color.Red;
            this.btn_end.Location = new System.Drawing.Point(478, 335);
            this.btn_end.Name = "btn_end";
            this.btn_end.Size = new System.Drawing.Size(75, 33);
            this.btn_end.TabIndex = 10;
            this.btn_end.Text = "&Ende";
            this.btn_end.UseVisualStyleBackColor = false;
            this.btn_end.Click += new System.EventHandler(this.btn_end_Click);
            // 
            // Zylinderberechnung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 422);
            this.Controls.Add(this.btn_end);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rbtn_Oberflaeche);
            this.Controls.Add(this.rbtn_Mantelflaeche);
            this.Controls.Add(this.rbtn_Volumen);
            this.Controls.Add(this.rbtn_Umfang);
            this.Controls.Add(this.rbtn_grundflaeche);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Zylinderberechnung";
            this.Text = "Zylinderberechnung";
            this.Load += new System.EventHandler(this.Zylinderberechnung_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtn_grundflaeche;
        private System.Windows.Forms.RadioButton rbtn_Umfang;
        private System.Windows.Forms.RadioButton rbtn_Volumen;
        private System.Windows.Forms.RadioButton rbtn_Mantelflaeche;
        private System.Windows.Forms.RadioButton rbtn_Oberflaeche;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_input_radius;
        private System.Windows.Forms.Label lbl_hoehe;
        private System.Windows.Forms.Label lbl_radius;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.TextBox txt_input_Hoehe;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_end;
    }
}

