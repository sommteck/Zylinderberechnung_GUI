using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zylinderberechnung_GUI
{
    public partial class Zylinderberechnung : Form
    {
        public Zylinderberechnung()
        {
            InitializeComponent();

            ToolTip tp = new ToolTip();
            tp.SetToolTip(btn_ok, "Berechnung ausführen");
            tp.SetToolTip(btn_clear, "Ein- und Ausgaben löschen");
            tp.SetToolTip(btn_end, "Programm beenden");
            tp.SetToolTip(rbtn_grundflaeche, "Zum Berechnen der Zylindergrundfläche anwählen");
            tp.SetToolTip(rbtn_Umfang, "Zum Berechnen des Zylinderumfangs anwählen");
            tp.SetToolTip(rbtn_Volumen, "Zum Berechnen des Zylindervolumens anwählen");
            tp.SetToolTip(rbtn_Mantelflaeche, "Zum Berechnen der Mantelfläche anwählen");
            tp.SetToolTip(rbtn_Oberflaeche, "Zum Berechnen der Zylindergesamtoberfläche");
        }

        private void btn_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            rbtn_grundflaeche.Checked = rbtn_Umfang.Checked = rbtn_Mantelflaeche.Checked = rbtn_Volumen.Checked = rbtn_Oberflaeche.Checked = false;
            lbl_hoehe.Visible = lbl_radius.Visible = false;
            txt_output.Clear();
            txt_input_Hoehe.Clear();
            txt_input_radius.Clear();
            txt_input_Hoehe.Visible = txt_input_radius.Visible = false;
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (rbtn_grundflaeche.Checked)
            {

                double grundflaeche, radius;
                try
                {
                    radius = Convert.ToDouble(txt_input_radius.Text);
                    grundflaeche = radius * radius * Math.PI;
                    txt_output.Text = grundflaeche.ToString("F4");
                }

                catch (Exception Fehler)
                {
                    MessageBox.Show(Fehler.Message);
                    txt_input_radius.Clear();
                    txt_input_radius.Focus();
                }
            }

            if (rbtn_Umfang.Checked)
            {

                double umfang, radius;
                try
                {
                    radius = Convert.ToDouble(txt_input_radius.Text);
                    umfang = radius * Math.PI * 2;
                    txt_output.Text = umfang.ToString("F4");
                }

                catch (Exception Fehler)
                {
                    MessageBox.Show(Fehler.Message);
                    txt_input_radius.Clear();
                    txt_input_radius.Focus();
                }
            }
            if (rbtn_Volumen.Checked)
            {

                double volumen, radius, hoehe;
                try
                {
                    radius = Convert.ToDouble(txt_input_radius.Text);
                    hoehe = Convert.ToDouble(txt_input_Hoehe.Text);
                    volumen = radius * radius * Math.PI * hoehe;
                    txt_output.Text = volumen.ToString("F4");
                }

                catch (Exception Fehler)
                {
                    MessageBox.Show(Fehler.Message);
                    txt_input_radius.Clear();
                    txt_input_radius.Focus();
                }
            }

            if (rbtn_Mantelflaeche.Checked)
            {

                double mantelflaeche, radius, hoehe;
                try
                {
                    radius = Convert.ToDouble(txt_input_radius.Text);
                    hoehe = Convert.ToDouble(txt_input_Hoehe.Text);
                    mantelflaeche = radius * radius * Math.PI * hoehe;
                    txt_output.Text = mantelflaeche.ToString("F4");
                }

                catch (Exception Fehler)
                {
                    MessageBox.Show(Fehler.Message);
                    txt_input_radius.Clear();
                    txt_input_radius.Focus();
                }
            }

            if (rbtn_Oberflaeche.Checked)
            {

                double oberflaeche, radius, hoehe;
                try
                {
                    radius = Convert.ToDouble(txt_input_radius.Text);
                    hoehe = Convert.ToDouble(txt_input_Hoehe.Text);
                    oberflaeche = (2 * Math.PI * Math.Pow(radius, 2)) + (2 * Math.PI * radius * hoehe);
                    txt_output.Text = oberflaeche.ToString("F4");
                }

                catch (Exception Fehler)
                {
                    MessageBox.Show(Fehler.Message);
                    txt_input_radius.Clear();
                    txt_input_radius.Focus();
                }
            }
        }

        private void rbtn_grundflaeche_CheckedChanged(object sender, EventArgs e)
        {
            txt_input_radius.Text = txt_input_Hoehe.Text = txt_output.Text = null;
            lbl_hoehe.Visible = txt_input_Hoehe.Visible = false;
            lbl_radius.Visible = txt_input_radius.Visible = true;
            txt_input_radius.Focus();
        }

        private void rbtn_Umfang_CheckedChanged(object sender, EventArgs e)
        {
            txt_input_radius.Text = txt_input_Hoehe.Text = txt_output.Text = null;
            lbl_hoehe.Visible = txt_input_Hoehe.Visible = false;
            lbl_radius.Visible = txt_input_radius.Visible = true;
            txt_input_radius.Focus();
        }

        private void rbtn_Volumen_CheckedChanged(object sender, EventArgs e)
        {
            txt_input_radius.Text = txt_input_Hoehe.Text = txt_output.Text = null;
            lbl_hoehe.Visible = lbl_radius.Visible = txt_input_Hoehe.Visible = txt_input_radius.Visible = true;
            txt_input_radius.Focus();
        }

        private void rbtn_Mantelflaeche_CheckedChanged(object sender, EventArgs e)
        {
            txt_input_radius.Text = txt_input_Hoehe.Text = txt_output.Text = null;
            lbl_hoehe.Visible = lbl_radius.Visible = txt_input_Hoehe.Visible = txt_input_radius.Visible = true;
            txt_input_radius.Focus();
        }

        private void rbtn_Oberflaeche_CheckedChanged(object sender, EventArgs e)
        {
            txt_input_radius.Text = txt_input_Hoehe.Text = txt_output.Text = null;
            lbl_hoehe.Visible = lbl_radius.Visible = txt_input_Hoehe.Visible = txt_input_radius.Visible = true;
            txt_input_radius.Focus();
        }

        private void Zylinderberechnung_Load(object sender, EventArgs e)
        {

        }
    }
}